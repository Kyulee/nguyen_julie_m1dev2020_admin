#!/bin/zsh

newUser=""
oldUser=""

function checkUserExist () {

    cut -d: -f1 /etc/passwd | grep -wi $1 
    if [[ $? -eq 0 ]]
    then
        echo "L'utilisateur $1 existe déja."
        return 1
    else
        echo "L'utilisateur $1 n'existe pas."
        return 0
    fi
}

function createUser() {
    echo "Quel est le nom de l'utilisateur ?"
    read newUser
    checkUserExist $newUser
    if [[ $? -eq 0 ]]
    then
        echo "Souhaitez vous créer l'utilisateur $newUser ?" 
        select on in "Oui" "Non"
            case $on in
            Oui ) 
                echo "L'utilisateur $newUser à été crée."
                sudo useradd $newUser
                sudo passwd $newUser
                exit;;
            Non ) exit;;
            esac
    else
    echo "Veuillez choisir un autre nom d'utilisateur."

    fi
}

function deleteUser () {
    echo "Quel est le nom de l'utilisateur ?"
    read oldUser
    checkUserExist $oldUser
    if [[ $? -eq 1 ]]
    then
        echo "Souhaitez vous supprimer l'utilisateur $oldUser ?" 
        select on in "Oui" "Non"
            case $on in
            Oui ) 
                echo "L'utilisateur $oldUser à été supprimé."
                sudo userdel $oldUser
                exit;;
            Non ) exit;;
            esac
    else
    echo "Veuillez choisir un autre nom d'utilisateur."
    fi
}

function usrMenu () {
    PS3="Que voulez-vous faire ? "
    select opt in "Créer un nouvel utilisateur" "Supprimer un utilisateur" "Annuler"
    do
        case $opt in
            "Créer un nouvel utilisateur")
                createUser
                ;;
            "Supprimer un utilisateur")
                deleteUser
                ;;
            "Annuler")
                break
                ;;
            *) echo "Ce n'est pas un choix." ;;
        esac
    done
}

usrMenu