#!/bin/zsh

newGroup=""
oldGroup=""

function checkGroupExist () {

    cut -d: -f1 /etc/group | grep -wi $1 

    if [[ $? -eq 0 ]]
    then
        echo "Le groupe $1 existe déja."
        return 1
    else
        echo "Le groupe $1 n'existe pas."
        return 0
    fi
}

function createGroup() {
    echo "Quel est le nom du groupe ?"
    read newGroup
    checkGroupExist $newGroup
    if [[ $? -eq 0 ]]
    then
        echo "Souhaitez vous créer le groupe $newGroup ?" 
        select on in "Oui" "Non"
            case $on in
            Oui ) 
                echo "Le groupe $newGroup à été crée."
                sudo groupadd $newGroup
                exit;;
            Non ) exit;;
            esac
    else
    echo "Veuillez choisir un autre nom de groupe."

    fi
}

function deleteGroup () {
    echo "Quel est le nom du groupe ?"
    read oldGroup
    checkGroupExist $oldGroup
    if [[ $? -eq 1 ]]
    then
        echo "Souhaitez vous supprimer le groupe $oldGroup ?" 
        select on in "Oui" "Non"
            case $on in
            Oui ) 
                echo "Le groupe $oldGroup à été supprimé."
                sudo groupdel $oldGroup
                exit;;
            Non ) exit;;
            esac
    else
    echo "Veuillez choisir un autre nom de groupe."
    fi
}

function usrMenu () {
    PS3="Que voulez-vous faire ? "
    select opt in "Créer un nouveau groupe" "Supprimer un groupe" "Annuler"
    do
        case $opt in
            "Créer un nouveau groupe")
                createGroup
                ;;
            "Supprimer un groupe")
                deleteGroup
                ;;
            "Annuler")
                break
                ;;
            *) echo "Ce n'est pas un choix" ;;
        esac
    done
}

usrMenu