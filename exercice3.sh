#!/bin/zsh

function checkUserExist () {

    cut -d: -f1 /etc/passwd | grep -wi $1 

    if [[ $? -eq 0 ]]
    then
        echo "L'utilisateur $1 existe déja."
    else
        echo "L'utilisateur $1 n'existe pas."
    fi
}

checkUserExist $1
