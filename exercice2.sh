#!/bin/zsh

function argAction() {
    if [[ ! -e $1 ]]
    then
        echo "Le fichier n'existe pas"
        exit 1
    fi
    
    case $2 in

        "copy"|"COPY" )
            echo "Le fichier $1 à été copié dans /tmp/script"
            cp $1 /tmp/script
            ;;

        "delete"|"DELETE" )
            rm -r $1
            echo "Le fichier $1 à bien été supprimé."
            ;;

        "read"|"READ" )
            echo "Voici le fichier $1 :"
            cat $1
            ;;
    esac


}

argAction $1 $2