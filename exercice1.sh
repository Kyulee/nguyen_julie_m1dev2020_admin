#!/bin/zsh

NBARG=$#
FILES=$@

function countArg () {
# Ici, on compte le nombre d'argument
    if [[ $NBARG -eq 0 ]]
    then 
        echo " Il n'y a pas de fichier"
        return 2
    else
        echo "Il y a $NBARG fichiers"
        isFileInListExist $*
    fi
}

function isFileInListExist () {
# Vérification de l'existance du fichier
    for file in $*
    do
        if [[ -e $file ]]
        then
            echo "Le fichier $file existe"
        else
            echo "Le fichier $file n'existe pas"
        fi
    done
}

countArg $*