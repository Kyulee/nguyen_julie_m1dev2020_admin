#!/bin/zsh

newFile=""
oldFile=""

function checkFileExist () {

    if [[ -e $1 ]]
    then
        echo "Le fichier $1 existe déja."
        return 1
    else
        echo "Le fichier $1 n'existe pas."
        return 0
    fi
}

function createFile() {
    echo "Quel est le nom du fichier ?"
    read newFile
    checkFileExist $newFile
    if [[ $? -eq 0 ]]
    then
        echo "Souhaitez vous créer le fichier $newFile ?" 
        select on in "Oui" "Non"
            case $on in
            Oui ) 
                echo "Le fichier $newFile a été crée."
                sudo mkdir $newFile
                exit;;
            Non )
                echo "Le fichier $newFile n'a pas été crée."
                exit;;
            esac
    else
    echo "Veuillez choisir un autre nom de fichier."

    fi
}

function deleteFile () {
    echo "Quel est le nom du fichier ?"
    read oldFile
    checkFileExist $oldFile
    if [[ $? -eq 1 ]]
    then
        echo "Souhaitez vous supprimer le fichier $oldFile ?" 
        select on in "Oui" "Non"
            case $on in
            Oui ) 
                echo "Le fichier $oldFile a été supprimé."
                sudo rm -r $oldFile
                exit;;
            Non ) 
                echo "Le fichier $oldFile a été conservé."
                exit;;
            esac
    else
    echo "Veuillez choisir un autre nom de fichier."
    fi
}

function usrMenu () {
    PS3="Que voulez-vous faire ? "
    select opt in "Créer un nouveau fichier" "Supprimer un fichier" "Annuler"
    do
        case $opt in
            "Créer un nouveau fichier")
                createFile
                ;;
            "Supprimer un fichier")
                deleteFile
                ;;
            "Annuler")
                break
                ;;
            *) echo "Ce n'est pas un choix." ;;
        esac
    done
}

usrMenu