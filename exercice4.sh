#!/bin/zsh

function checkGroupExist () {

    cut -d: -f1 /etc/group | grep -wi $1 

    if [[ $? -eq 0 ]]
    then
        echo "Le groupe $1 existe déja."
    else
        echo "Le groupe $1 n'existe pas."
    fi
}

checkGroupExist $1
