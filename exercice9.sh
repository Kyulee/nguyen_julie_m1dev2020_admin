#!/bin/zsh

inputFile=""
username=""
groupname=""
allRight=0

function usrMenu () {
    PS3="Que voulez-vous faire ? "
    select opt in "Modifier les droits d'un fichier" "Annuler"
    do
        case $opt in
            "Modifier les droits d'un fichier")
                modifyRightFile
                ;;
            "Annuler")
                break
                ;;
            *) echo "Ce n'est pas un choix." ;;
        esac
    done
}

function modifyRightFile() {
    echo "Quel est le nom du fichier ?"
    read inputFile
    checkFileExist $inputFile
    if [[ $? -eq 0 ]]
    then
        echo "Souhaitez vous modifier les droits du fichier $inputFile ?" 
        select on in "Oui" "Non"
            case $on in
            Oui ) 
                askRights
                echo "Les droits du fichier $inputFile ont été modifiés."
                exit
                ;;
            Non )
                echo "Les droits du fichier $inputFile n'ont pas été modifiés."
                exit
                ;;
            *) echo "Ce n'est pas un choix." ;;
            esac
    else
    echo "Veuillez choisir un autre fichier qui existe."
    fi
}

function checkFileExist () {

    if [[ -e $1 ]]
    then
        echo "Le fichier $1 existe déja."
        return 0
    else
        echo "Le fichier $1 n'existe pas."
        return 1
    fi
}


function askRights () {
    # Quels droits pour l'user ?
    echo "Pour quel utilisateur ?"
    read username
    chown $username $inputFile
    getRights
    allRight=$?
    # Quels droits pour le group ?
    echo "Pour quel groupe ?"
    read groupname
    chown :$groupname $inputFile
    getRights
    allRight+=$?
    # ...
    echo "Pour les autres ?"
    getRights
    allRight+=$?
    echo $allRight
    chmod $allRight $inputFile
    echo $?
}

function getRights () {
    rights=0
    read "response?Droit de lecture du fichier ? (Y/n)[Y]"
    case $response in 
    [yY] | "" )
        rights=`expr $rights + 4`
        echo "Le droit de lecture a été ajouté."
        ;;
    [nN] )
        echo "Le droit de lecture n'est pas ajouté."
        ;;
    *) echo "Ce n'est pas un choix." ;;
    esac
    # Est ce que user peut lire ?
    # get oui ou non
    # si oui
    # right + 4
    read "response?Droit d'écriture du fichier ? (Y/n)[Y]" 
    case $response in 
    [yY] | "" )
        rights=`expr $rights + 2`
        echo "Le droit d'écriture a été ajouté."
        ;;
    [nN]* )
        echo "Le droit d'écriture n'est pas ajouté."
        ;;
    *) echo "Ce n'est pas un choix." ;;
    esac
    # ecrire ?
    # right + 2
    read "response?Droit d'éxécution du fichier ? (Y/n)[Y] " 
    case $response in 
    [yY] | "" )
        rights=`expr $rights + 1`
        echo "Le droit d'éxécution a été ajouté."
        ;;
    [nN]* )
        echo "Le droit d'éxécution n'est pas ajouté."
        ;;
    *) echo "Ce n'est pas un choix." ;;
    esac
    # exec ?
    # right = 1
    return rights
    # return right
}

usrMenu