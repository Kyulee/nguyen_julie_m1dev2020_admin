#!/bin/zsh

inputFile=""
username=""
groupname=""

function checkFileExist () {

    if [[ -e $1 ]]
    then
        echo "Le fichier $1 existe déja."
        return 0
    else
        echo "Le fichier $1 n'existe pas."
        return 1
    fi
}

function modifyRightFile() {
    echo "Quel est le nom du fichier ?"
    read inputFile
    checkFileExist $inputFile
    if [[ $? -eq 0 ]]
    then
        echo "Souhaitez vous modifier les droits du fichier $inputFile ?" 
        select on in "Oui" "Non"
            case $on in
            Oui ) 
                echo "Quel est l'utilisateur qui accèdera au fichier ?"
                read username
                echo "Quel est le groupe qui accèdera au fichier ?"
                read groupname
                chown $username:$groupname $inputFile
                chmod u+rwx,g+rwx $inputFile
                echo "Le fichier $inputFile ont désormais tous les droits sous l'utilisateur $username et le groupe $groupname."
                exit;;
            Non )
                echo "Les droits du fichier $inputFile n'ont pas été modifiés."
                exit;;
            esac
    else
    echo "Veuillez choisir un autre fichier qui existe."

    fi
}

function usrMenu () {
    PS3="Que voulez-vous faire ? "
    select opt in "Modifier les droits d'un fichier" "Annuler"
    do
        case $opt in
            "Modifier les droits d'un fichier")
                modifyRightFile
                ;;
            "Annuler")
                break
                ;;
            *) echo "Ce n'est pas un choix." ;;
        esac
    done
}

usrMenu